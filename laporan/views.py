from django.shortcuts import render
from sepeda.models import *
from django.core.paginator import Paginator

content = {}


def daftar_laporan(request):
    list_laporan = Laporan.objects.raw(
        'select * from laporan natural join peminjaman join anggota on no_kartu_anggota = no_kartu natural join person')
    pagination = Paginator(list_laporan, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'laporan.html', content)

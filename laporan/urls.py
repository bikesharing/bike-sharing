from django.urls import path
from .views import *

app_name = 'laporan'
urlpatterns = [
    path('', daftar_laporan, name="list-laporan"),
]
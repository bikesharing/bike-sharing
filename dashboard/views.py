from django.shortcuts import render


# Create your views here.


def dashboard(request):
    return render(request, 'dashboard/dashboard.html')


def peminjaman(request):
    return render(request, "dashboard/peminjaman/peminjaman.html")


def laporan(request):
    return render(request, "dashboard/laporan/laporan.html")


def voucher(request):
    return render(request, 'dashboard/voucher/voucher.html')


def update_voucher(request):
    return render(request, 'dashboard/voucher/update-voucher.html')


def register_voucher(request):
    return render(request, 'dashboard/voucher/register-voucher.html')

from django.urls import path
from .views import *

urlpatterns = [
    path('', dashboard, name="dashboard"),
    path('peminjaman/', peminjaman, name="admin-list-peminjaman"),
    path('voucher/', voucher, name='admin-list-voucher'),
    path('voucher/update', update_voucher, name="admin-update-voucher"),
    path('voucher/register', register_voucher, name="admin-register-voucher"),
    path('laporan/', laporan, name="admin-list-laporan"),
]

from django import forms
from django.db import connection


def list_stasiun():
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT nama from stasiun'
        )
        row = cursor.fetchall()
    return row


class orderFilter(forms.Form):
    list_stasiun = forms.ChoiceField(required=False, label="", widget=forms.Select(
        attrs={'class': 'form-control'}, choices=list_stasiun()))
    judul = forms.CharField(label="", required=True, max_length=20,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control'}))
    deskripsi = forms.CharField(label="", required=False, max_length=200,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control'}))
    isFree = forms.ChoiceField(required=False, label="Filter Stasiun", widget=forms.Select(
        attrs={'class': 'form-control'}), choices=(('Ya', 'Ya'), ('Tidak', 'Tidak')))
    tgl_mulai = forms.DateField(label="", required=True,
                                widget=forms.DateInput(attrs={'class': 'form-control'}))
    tgl_selesai = forms.DateField(label="", required=True,
                                  widget=forms.DateInput(attrs={'class': 'form-control'}))

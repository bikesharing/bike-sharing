from django.urls import path
from .views import *

app_name = 'acara'
urlpatterns = [
    path('', acara, name="list-acara"),
    path('sortasc/', sortbyacaraASC, name="sort-asc"),
    path('sortdesc/', sortbyacaraDESC, name="sort-desc"),
    path('petugas/register/', register_acara, name="admin-register-acara"),
    path('petugas/update/', update_acara, name="admin-update-acara"),
]

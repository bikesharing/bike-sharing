from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages
from sepeda.models import *
from .forms import *
from registration.check import required_login
from django.core.paginator import Paginator

content = {}


# Create your views here.

@required_login
def acara(request):
    list_acara = list(Acara.objects.raw(
        'SELECT * FROM ACARA NATURAL JOIN ACARA_STASIUN NATURAL JOIN STASIUN'))
    pagination = Paginator(list_acara, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'acara/acara.html', content)


@required_login
def sortbyacaraASC(request):
    list_acara = list(Acara.objects.raw(
        'SELECT * FROM ACARA NATURAL JOIN ACARA_STASIUN NATURAL JOIN STASIUN ORDER BY judul ASC'))
    pagination = Paginator(list_acara, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'acara/acara.html', content)


@required_login
def sortbyacaraDESC(request):
    list_acara = list(Acara.objects.raw(
        'SELECT * FROM ACARA NATURAL JOIN ACARA_STASIUN NATURAL JOIN STASIUN ORDER BY judul DESC'))
    pagination = Paginator(list_acara, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'acara/acara.html', content)


def register_acara(request):
    return render(request, 'acara/register-acara.html')


def update_acara(request):
    return render(request, 'acara/update-acara.html')

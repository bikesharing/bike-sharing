from django.shortcuts import render
from sepeda.models import *
from django.core.paginator import Paginator

content = {}


# Create your views here.


def petugas(request):
    list_petugas = Petugas.objects.raw(
        'SELECT P.ktp, S.id_stasiun, PR.nama AS nama_petugas, S.nama AS nama_stasiun, start_datetime, end_datetime FROM ((PETUGAS P join PENUGASAN PT on P.ktp = PT.ktp) JOIN STASIUN S ON PT.id_stasiun = S.id_stasiun) JOIN PERSON PR ON P.ktp = PR.ktp')
    pagination = Paginator(list_petugas, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'petugas/petugas.html', content)


def sortbypetugasASC(request):
    list_petugas = Petugas.objects.raw(
        'SELECT P.ktp, S.id_stasiun, PR.nama AS nama_petugas, S.nama AS nama_stasiun, start_datetime, end_datetime FROM ((PETUGAS P join PENUGASAN PT on P.ktp = PT.ktp) JOIN STASIUN S ON PT.id_stasiun = S.id_stasiun) JOIN PERSON PR ON P.ktp = PR.ktp ORDER BY PR.nama ASC')
    pagination = Paginator(list_petugas, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'petugas/petugas.html', content)


def sortbypetugasDESC(request):
    list_petugas = Petugas.objects.raw(
        'SELECT P.ktp, S.id_stasiun, PR.nama AS nama_petugas, S.nama AS nama_stasiun, start_datetime, end_datetime FROM ((PETUGAS P join PENUGASAN PT on P.ktp = PT.ktp) JOIN STASIUN S ON PT.id_stasiun = S.id_stasiun) JOIN PERSON PR ON P.ktp = PR.ktp ORDER BY PR.nama DESC')
    pagination = Paginator(list_petugas, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'petugas/petugas.html', content)


def register_petugas(request):
    return render(request, 'petugas/register-petugas.html')


def update_petugas(request):
    return render(request, 'petugas/update-petugas.html')

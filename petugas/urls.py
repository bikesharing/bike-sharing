from django.urls import path
from .views import *

app_name = 'petugas'
urlpatterns = [
    path('', petugas, name="list-petugas"),
    path('sortasc/', sortbypetugasASC, name="sort-asc"),
    path('sortdesc/', sortbypetugasDESC, name="sort-desc"),
    path('petugas/register/', register_petugas, name="admin-register-petugas"),
    path('petugas/update/', update_petugas, name="admin-update-petugas"),
]

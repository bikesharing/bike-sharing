from django.urls import path
from voucher import views

app_name = 'voucher'
urlpatterns = [
    path('api-voucher/', views.api_voucher, name='api-voucher'),
    path('createvoucher/', views.createvoucher, name='create-voucher'),
    path('listvoucher/', views.voucher, name='list-voucher'),
    path('hapus-voucher/<str:nama>', views.hapus_voucher, name='hapus-voucher'),
    path('tambah-voucher/', views.tambah_voucher, name='tambah-voucher'),
    path('update-voucher/<str:nama>', views.update_voucher, name='update-voucher'),
    # path('', dashboardvoucher, name="list-voucher"),
]

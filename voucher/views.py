from django.shortcuts import render, get_object_or_404
from sepeda.models import *
from django.core.paginator import Paginator
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse, HttpResponseForbidden
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.
content = {}


def createvoucher(request):
    url = "voucher/register-voucher.html"
    return render(request, url)


def listvoucher(request):
    url = "voucher/voucher.html"
    return render(request, url)


def voucher(request):
    list_voucher = Voucher.objects.raw('SELECT * FROM VOUCHER NATURAL JOIN ANGGOTA')
    pagination = Paginator(list_voucher, 10)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'voucher/voucher.html', content)


def api_voucher(request):
    c = connection.cursor()
    c.execute("SELECT * FROM voucher;")
    result = c.fetchall()
    jsonObject = [dict(zip([key[0] for key in c.description], row))
                  for row in result]
    c.close()
    return HttpResponse(json.dumps({"data": jsonObject}), content_type="application/json")


def tambah_voucher(request):
    if request.method == "POST":
        c = connection.cursor()
        try:
            id_voucher = request.POST.get('id_voucher')
            nama = request.POST.get('nama')
            kategori = request.POST.get('kategori')
            nilai_poin = float(request.POST.get('poin'))
            deskripsi = request.POST.get('deskripsi')
            no_kartu_anggota = request.POST.get('no_kartu_anggota')
            c.execute("INSERT INTO voucher(id_voucher, nama, kategori, nilai_poin, deskripsi, no_kartu_anggota) \
                 VALUES(%s, %s, %s)", [id_voucher, nama, kategori, nilai_poin, deskripsi, no_kartu_anggota])
        finally:
            c.close()
        return HttpResponseRedirect(reverse('level:listvoucher'))
    else:
        return HttpResponseForbidden()


def update_voucher(request, nama):
    url = "voucher/update-voucher.html"
    c = connection.cursor()
    if request.method == "POST":
        try:
            old_nama = nama
            nama = request.POST.get('level')
            kategori = request.POST.get('kategori')
            nilai_poin = float(request.POST.get('poin'))
            deskripsi = request.POST.get('deskripsi')
            c.execute("UPDATE voucher set nama=%s, kategori=%s, nilai_poin=%s,\
                 deskripsi=%s WHERE nama=%s", [nama, kategori, nilai_poin, deskripsi, old_nama])
        finally:
            c.close()
        return HttpResponseRedirect(reverse('level:listvoucher'))

    c.execute("SELECT * FROM voucher WHERE nama=%s",
              [nama])
    result = c.fetchall()
    jsonObject = [dict(zip([key[0] for key in c.description], row))
                  for row in result]
    c.close()
    return render(request, url, {"data": jsonObject})


@csrf_exempt
def hapus_voucher(request, nama):
    c = connection.cursor()
    try:
        c.execute(
            "DELETE FROM voucher WHERE nama=%s", [nama])
    finally:
        c.close()
    return HttpResponseRedirect(reverse('voucher:daftar-voucher'))

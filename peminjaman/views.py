from django.shortcuts import render, get_object_or_404, HttpResponse
from sepeda.models import *
from django.core.paginator import *
from registration.check import required_login
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse, HttpResponseForbidden
from django.db import connection
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json

content = {}


@required_login
def peminjaman(request):
    list_peminjaman = Peminjaman.objects.raw('SELECT * FROM PEMINJAMAN')
    pagination = Paginator(list_peminjaman, 10)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'peminjaman/peminjaman.html', content)


@required_login
def register_peminjaman(request):
    return render(request, "peminjaman/register-peminjaman.html")

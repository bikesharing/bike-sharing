from django.urls import path
from .views import *

app_name = 'peminjaman'
urlpatterns = [
    path('', peminjaman, name="list-peminjaman"),
    path('register/', register_peminjaman, name="register-peminjaman"),
]

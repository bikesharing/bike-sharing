from django.forms import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.db import connection
from django.contrib import messages
from django.core.paginator import Paginator
from sepeda.models import *
from .check import required_login
from .forms import *
from random import randint

response = {}


def random_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


def registration(request):
    if 'role' in request.session:
        if request.session['role'] == 'Petugas':
            return HttpResponseRedirect(reverse('acara:list-acara'))
        else:
            return HttpResponseRedirect(reverse('stasiun:list-stasiun'))
    if request.method == 'POST':
        form = SignUpForm(request.POST, prefix="form")
        if form.is_valid():
            no_ktp_exist = list(Person.objects.raw(
                'SELECT * FROM PERSON WHERE ktp = %s', [form.cleaned_data.get('ktp')]))
            email_exist = list(Person.objects.raw(
                'SELECT * FROM PERSON WHERE email = %s', [form.cleaned_data.get('email')]))
            if no_ktp_exist:
                messages.error(request, 'No KTP Anda sudah terdaftar')
            if email_exist:
                messages.error(request, 'Email Anda sudah terdaftar')
            if email_exist or no_ktp_exist:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            registration_petugas_anggota(form)
            return HttpResponseRedirect(reverse('auth:login'))
        messages.error(request, 'Form tidak valid')

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    form = SignUpForm(prefix="form")
    return render(request, 'registration/signup.html', {'signup_form': form})


def registration_petugas_anggota(form):
    result = [form.cleaned_data.get('ktp'), form.cleaned_data.get('email'),
              form.cleaned_data.get(
                  'nama'), form.cleaned_data.get('alamat'),
              form.cleaned_data.get('tgl_lahir'), form.cleaned_data.get('no_telp')]
    peran = form.cleaned_data.get('peran')
    with connection.cursor() as cursor:
        cursor.execute(
            "INSERT INTO PERSON values(%s, %s, %s, %s, %s, %s)", result)
        if peran == "2":
            cursor.execute("INSERT INTO PETUGAS values(%s, '30000')",
                           [form.cleaned_data.get('ktp'), ])
        else:
            cursor.execute("INSERT INTO ANGGOTA values(%s, 0, 0, %s)",
                           [str(random_N_digits(10)), form.cleaned_data.get('ktp')])


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = Person.objects.raw(
                'SELECT * FROM PERSON WHERE ktp = %s and email = %s',
                [form.cleaned_data.get('ktp'), form.cleaned_data.get('email'), ])
            if list(user):
                request.session['ktp'] = form.cleaned_data.get(
                    'ktp')
                if list(Petugas.objects.raw('SELECT * FROM PETUGAS WHERE ktp = %s', [form.cleaned_data.get('ktp')])):
                    request.session['role'] = 'Petugas'
                    return HttpResponseRedirect(reverse('acara:list-acara'))
                else:
                    request.session['role'] = 'Anggota'
                    return HttpResponseRedirect(reverse('stasiun:list-stasiun'))
            else:
                messages.error(request, 'Email atau Nomor KTP salah')
        else:
            messages.error(request, 'Form tidak valid!')
    else:
        response['login_form'] = LoginForm
    if 'role' in request.session:
        if request.session['role'] == 'Petugas':
            return HttpResponseRedirect(reverse('acara:list-acara'))
        else:
            return HttpResponseRedirect(reverse('stasiun:list-stasiun'))
    return render(request, 'registration/login.html', response)


def signup(request):
    response['signup_form'] = SignUpForm
    return render(request, 'registration/signup.html', response)


@required_login
def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('auth:login'))

from django.urls import path
from .views import *

app_name = 'auth'
urlpatterns = [
    path('signup/', registration, name="registration"),
    path('logout/', logout, name="logout"),
    path('', login, name="login"),
]

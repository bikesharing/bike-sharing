from django import forms
from registration.choices import *


class LoginForm(forms.Form):
    ktp_attrs = {'id': 'id_ktp',
                 'type': 'text',
                 'class': 'login-input-form',
                 'placeholder': 'NOMOR KTP'}

    email_attrs = {'id': 'id_email',
                   'type': 'email',
                   'class': 'login-input-form',
                   'placeholder': 'EMAIL'}

    ktp = forms.CharField(label="", required=True, max_length=20,
                          widget=forms.TextInput(attrs=ktp_attrs))
    email = forms.EmailField(label="", required=True, max_length=50,
                             widget=forms.EmailInput(attrs=email_attrs))


class SignUpForm(forms.Form):
    ktp_attrs = {'id': 'id_ktp',
                 'type': 'text',
                 'class': 'signup-input-form',
                 'placeholder': 'NOMOR KTP'}

    email_attrs = {'id': 'id_email',
                   'type': 'text',
                   'class': 'signup-input-form',
                   'placeholder': 'EMAIL'}

    nama_attrs = {'id': 'id_nama',
                  'type': 'text',
                  'class': 'signup-input-form',
                  'placeholder': 'NAMA LENGKAP'}

    alamat_attrs = {'id': 'id_alamat',
                    'type': 'text',
                    'class': 'signup-input-form',
                    'placeholder': 'ALAMAT'}

    tgl_lahir_attrs = {'id': 'id_tgl_lahir',
                       'type': 'date',
                       'class': 'signup-input-form',
                       'placeholder': 'TANGGAL LAHIR'}

    no_telp_attrs = {'id': 'id_no_telp',
                     'type': 'text',
                     'class': 'signup-input-form',
                     'placeholder': 'NOMOR TELEPON'}

    peran_attrs = {'id': 'id_peran',
                   'type': 'select',
                   'class': 'peran-input'}

    ktp = forms.CharField(label="", required=True, max_length=20,
                          widget=forms.TextInput(attrs=ktp_attrs))
    email = forms.EmailField(label="", required=True, max_length=50,
                             widget=forms.EmailInput(attrs=email_attrs))
    nama = forms.CharField(label="", required=True, max_length=50,
                           widget=forms.TextInput(attrs=nama_attrs))
    alamat = forms.CharField(label="", required=False, max_length=200,
                             widget=forms.TextInput(attrs=alamat_attrs))
    tgl_lahir = forms.DateField(label="", required=True,
                                widget=forms.DateInput(attrs=tgl_lahir_attrs))
    no_telp = forms.CharField(label="", required=False, max_length=20,
                              widget=forms.TextInput(attrs=no_telp_attrs))
    peran = forms.ChoiceField(choices=ROLE_CHOICES, label="Pilih Peran", initial='',
                              widget=forms.Select(), required=True)

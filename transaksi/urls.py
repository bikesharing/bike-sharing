from django.urls import path
from .views import *

urlpatterns = [
    path('', daftartransaksi, name="list-transaksi"),
    path('topup', topup, name="topup"),
]
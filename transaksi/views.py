from django.shortcuts import render
from .forms import TopUpForm


response = {}
def daftartransaksi(request):
    return render(request, 'transaksi.html')

def topup(request):
    response['topup_form'] = TopUpForm
    return render(request, 'topup.html', response)

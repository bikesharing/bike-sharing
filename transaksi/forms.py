from django import forms

class TopUpForm(forms.Form):
    nominal_attrs = {'id': 'id_nominal', 
                'type': 'number', 
                'class': 'nominal-input-form', 
                'placeholder': '   NOMINAL'}

    nominal = forms.FloatField(label="", required=True, 
        widget=forms.NumberInput(attrs=nominal_attrs))
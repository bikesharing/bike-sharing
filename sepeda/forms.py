from django import forms
from sepeda.models import *


def render_choices():
    choices = []
    query = list(Stasiun.objects.raw('SELECT * FROM STASIUN'))
    for i in query:
        choices.append([i.nama, i.nama])
    return choices


class SepedaForm(forms.Form):
    merk_attrs = {'id': 'id_merk',
                  'type': 'text',
                  'class': 'form-control',
                  'placeholder': 'MERK'}

    jenis_attrs = {'id': 'id_jenis',
                   'type': 'text',
                   'class': 'form-control',
                   'placeholder': 'JENIS'}

    merk = forms.CharField(label="", required=True, max_length=20,
                           widget=forms.TextInput(attrs=merk_attrs))
    jenis = forms.EmailField(label="", required=True, max_length=50,
                             widget=forms.TextInput(attrs=jenis_attrs))
    stasiun = forms.ChoiceField(choices=render_choices(), label="", initial='',
                                widget=forms.Select(), required=True)
    status = forms.ChoiceField(choices=(('TERSEDIA', 'TERSEDIA'), ('TIDAK TERSEDIA', 'TIDAK TERSEDIA')), label="",
                               initial='', widget=forms.Select(), required=True)
    penyumbang = forms.ChoiceField()

from django.urls import path
from .views import *

app_name = 'sepeda'
urlpatterns = [
    path('', sepeda, name="list-sepeda"),
    path('sortasc/', sortbysepedaASC, name="sort-asc"),
    path('sortdesc/', sortbysepedaDESC, name="sort-desc"),
    path('register/', register_sepeda, name="register-sepeda"),
    path('update/<str:nomor>/', update_sepeda, name="update-sepeda"),
]

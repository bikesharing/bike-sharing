from django.shortcuts import render
from .models import *
from django.core.paginator import Paginator
from registration.check import required_login

content = {}


# Create your views here.
@required_login
def sepeda(request):
    list_sepeda = Sepeda.objects.raw('SELECT * FROM SEPEDA')
    pagination = Paginator(list_sepeda, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'sepeda/sepeda.html', content)


@required_login
def sortbysepedaASC(request):
    list_sepeda = Sepeda.objects.raw('SELECT * FROM SEPEDA ORDER BY MERK ASC')
    pagination = Paginator(list_sepeda, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'sepeda/sepeda.html', content)


@required_login
def sortbysepedaDESC(request):
    list_sepeda = Sepeda.objects.raw('SELECT * FROM SEPEDA ORDER BY MERK DESC')
    pagination = Paginator(list_sepeda, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'sepeda/sepeda.html', content)


@required_login
def register_sepeda(request):
    if request.method == 'POST':
        merk = request.POST['merk']
        jenis = request.POST['jenis']
    list_stasiun = list(Stasiun.objects.raw('SELECT * FROM STASIUN'))
    content['nama_stasiun'] = list_stasiun
    return render(request, 'sepeda/register-sepeda.html', content)


@required_login
def update_sepeda(request, nomor):
    return render(request, 'sepeda/update-sepeda.html')

from django.shortcuts import render
from sepeda.models import *
from django.core.paginator import Paginator
from django.db import connection
from registration.views import random_N_digits
from registration.check import required_login
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
content = {}


@required_login
def stasiun(request):
    list_stasiun = Stasiun.objects.raw('SELECT * FROM STASIUN')
    pagination = Paginator(list_stasiun, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'stasiun/stasiun.html', content)


@required_login
def sortbystasiunASC(request):
    list_stasiun = Stasiun.objects.raw(
        'SELECT * FROM STASIUN ORDER BY nama ASC')
    pagination = Paginator(list_stasiun, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'stasiun/stasiun.html', content)


@required_login
def sortbystasiunDESC(request):
    list_stasiun = Stasiun.objects.raw(
        'SELECT * FROM STASIUN ORDER BY nama DESC')
    pagination = Paginator(list_stasiun, 6)
    page = request.GET.get('page')
    items = pagination.get_page(page)
    content['items'] = items
    return render(request, 'stasiun/stasiun.html', content)


@required_login
def register_stasiun(request):
    if request.method == 'POST':
        name = request.POST['nama']
        alamat = request.POST['alamat']
        lat = request.POST['lat']
        long = request.POST['long']
        id_stasiun = str(random_N_digits(3))
        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO STASIUN VALUES (%s, %s, %s, %s, %s)',
                           [id_stasiun, alamat, lat, long, name])
        return HttpResponseRedirect(reverse('stasiun:list-stasiun'))
    return render(request, 'stasiun/register-stasiun.html', content)


@required_login
def update_stasiun(request, nomor):
    return render(request, 'stasiun/update-stasiun.html')

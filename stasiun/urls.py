from django.urls import path
from .views import *

app_name = 'stasiun'
urlpatterns = [
    path('', stasiun, name="list-stasiun"),
    path('sortasc/', sortbystasiunASC, name="sort-asc"),
    path('sortdesc/', sortbystasiunDESC, name="sort-desc"),
    path('register/', register_stasiun, name="register-stasiun"),
    path('update/<str:nomor>/', update_stasiun, name="update-stasiun"),
]
